package vn.com.lending.publics.branch.event;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.kafka.common.serialization.Deserializer;

import java.sql.Timestamp;

@Data
@AllArgsConstructor
@NoArgsConstructor

public class CreateBranchEvent implements Deserializer {
    private String branchId;
    private String name;
    private String phoneNumber;
    private String province;
    private String district;
    private String address;
    private String representative;
    private Integer status;
    private Timestamp createTime;
    private Timestamp updateTime;
    private String userCreated;
    private String userUpdated;

    @Override
    public CreateBranchEvent deserialize(String topic, byte[] data) {
        ObjectMapper mapper = new ObjectMapper();
        CreateBranchEvent event = null;
        try {
            event = mapper.readValue(data, CreateBranchEvent.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return event;
    }
}
